@extends('layout.master')

@section('judul')
    Halaman Edit cast
@endsection

@section('content')
    <form method="POST" action="/cast/{{$cast->id}}">
        @csrf
        @method('PUT')
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" value="{{$cast->nama}}" class="form-control">
        </div>

        <div class="form-group">
            <label class="form-group">Umur</label>
            <input type="text" name="umur" value="{{$cast->umur}}" class="form-control">
        </div>

        <div class="form-group">
            <label class="form-group">Bio</label>
            <textarea class="form-control" name="bio" rows="3">{{$cast->bio}}</textarea>
        </div>

        {{--         <div class="mb-3 form-check">
        <input type="checkbox" class="form-check-input" id="exampleCheck1">
        <label class="form-check-label" for="exampleCheck1">Check me out</label>
    </div> --}}

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
