@extends('layout.master')
@section('judul')
Halaman Bio Data
@endsection
@section('content')
<a href="/">Kembali ke Home</a>

<form action="/home" method="POST" style="margin: 30px">
    @csrf
    <label for="">First Name</label> <br>
    <input type="text" name="fname"> <br> <br>

    <label for="">last name</label> <br>
    <input type="text" name="lname"> <br> <br>

    <label for="">Gender</label><br>
    <input type="radio" name="gender">Male <br>
    <input type="radio" name="gender">Female <br>
    <input type="radio" name="gender">Other <br><br>

    <label for="">Nationality</label><br>
    <select name="nation" id="" required>
        <option value="1">Indonesia</option>
        <option value="2">USA</option>
        <option value="3">France</option>
    </select><br><br>

    <label for="">Language Spoken</label><br>
    <input type="checkbox" value="1" name="indonesia">Indonesia <br>
    <input type="checkbox" value="2" name="english">English <br>
    <input type="checkbox" value="3" name="other">Other <br><br>

    <label for="">Bio</label><br>
    <Textarea name="bio" cols="30" rows="10" required></Textarea><br>
    <input type="submit" value="daftar">
</form>
@endsection
