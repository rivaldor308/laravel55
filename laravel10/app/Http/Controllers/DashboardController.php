<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index() {
        return view('index');
    }

    public function daftar()
    {
        return view('page.biodata');
    }

    public function home(Request $request)
    {
        $namadepan = $request->input('fname'); 
        $namabelakang = $request->input('lname'); 

        
        return view('page.home', ["namadepan" => $namadepan, "namabelakang" => $namabelakang]);
    }
}
